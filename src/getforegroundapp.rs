use std::collections::HashSet;
use std::process::Command;

// Common function that delegates to the appropriate platform-specific implementation
pub fn get_foreground_process_ids() -> HashSet<i32> {
    #[cfg(any(target_os = "linux", target_os = "windows"))]
    return procfs_process_info::get_foreground_process_ids();

    #[cfg(target_os = "macos")]
    return macos_process_info::get_foreground_process_ids();
}

// Module containing platform-specific process information logic
#[cfg(any(target_os = "linux", target_os = "windows"))]
mod procfs_process_info {
    use procfs::process::{FDInfo, FDTarget, Process};

    // Function to get the set of foreground process IDs on Linux and Windows
    pub fn get_foreground_process_ids() -> HashSet<i32> {
        let terminal_fd_info = match Process::myself() {
            Ok(process) => process.fd,
            Err(_) => return HashSet::new(),
        };

        let mut foreground_process_ids = HashSet::new();
        for fd_info in terminal_fd_info {
            if let FDTarget::Process(process_info) = fd_info.target {
                foreground_process_ids.insert(process_info.pid);
            }
        }

        foreground_process_ids
    }
}
#[cfg(target_os = "macos")]
mod macos_process_info {
    use std::collections::HashSet;
    use std::process::Command;
    // Function to get the set of foreground process IDs on macOS
    pub fn get_foreground_process_ids() -> HashSet<i32> {
        let output = Command::new("sh")
            .arg("-c")
            .arg("lsappinfo info -only pid $(lsappinfo front)")
            .output()
            .expect("Failed to execute command");

        let pid: i32 = String::from_utf8(output.stdout)
            .unwrap()
            .trim()
            .parse()
            .unwrap();

        let mut foreground_process_ids = HashSet::new();
        foreground_process_ids.insert(pid);

        foreground_process_ids
    }
}
