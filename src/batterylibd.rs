extern crate battery;
extern crate futures;
extern crate libc;
extern crate tokio;

use std::error::Error;
use std::ffi::CString;
use std::io::Read;
use std::mem::MaybeUninit;
use std::os::raw::{c_char, c_int, c_void};
use std::os::unix::io::AsRawFd;
use std::os::unix::io::RawFd;
use std::pin::Pin;
use std::result::Result;
use std::task::Context;
use std::task::Poll;

use tokio::io::unix::AsyncFd;
use tokio::io::Interest;
use tokio::io::ReadBuf;
use tokio::io::{AsyncRead, AsyncReadExt};

use futures::ready;

pub const KEY: &str = "com.apple.system.powersources.timeremaining";

struct NotifyFd {
    fd: RawFd,
    pub token: c_int,
}

impl NotifyFd {
    pub fn new(key: &str) -> Result<Self, Box<dyn Error>> {
        let mut token = MaybeUninit::<c_int>::uninit();
        let mut nfd = MaybeUninit::<RawFd>::uninit();
        unsafe {
            let key = CString::new(key).unwrap();
            let r = notify_register_file_descriptor(
                key.as_ptr(),
                nfd.as_mut_ptr(),
                0,
                token.as_mut_ptr(),
            );
            if r != 0 {
                return Err("notify_register_file_descriptor failed".into());
            }
        }
        let token = unsafe { token.assume_init() };
        let nfd = unsafe { nfd.assume_init() };

        return Ok(NotifyFd {
            fd: nfd,
            token: token,
        });
    }
}

impl Read for NotifyFd {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        unsafe {
            let r = libc::read(self.fd, buf.as_mut_ptr() as *mut c_void, buf.len());
            if r == -1 {
                return Err(std::io::Error::last_os_error());
            } else {
                return Ok(r as usize);
            }
        }
    }
}

impl Drop for NotifyFd {
    fn drop(&mut self) {
        unsafe {
            let r = notify_cancel(self.token);
            if r != 0 {
                panic!("notify_cancel failed");
            }
        }
    }
}

// Needed for integration with Tokio
impl AsRawFd for NotifyFd {
    fn as_raw_fd(&self) -> RawFd {
        return self.fd;
    }
}

pub struct AsyncNotifyFd {
    inner: AsyncFd<NotifyFd>,
    pub token: c_int,
}

impl AsyncNotifyFd {
    pub fn new(key: &str) -> Result<Self, Box<dyn Error>> {
        let mut nfd = NotifyFd::new(key)?;

        // Suspend the events while we adjust the fd
        unsafe {
            let r = notify_suspend(nfd.token);
            if r != 0 {
                return Err("notify_suspend failed".into());
            }
        }

        // Set the file descriptor in non blocking mode
        unsafe {
            let flags = libc::fcntl(nfd.fd, libc::F_GETFL);
            let r = libc::fcntl(nfd.fd, libc::F_SETFL, flags | libc::O_NONBLOCK);
            if r != 0 {
                return Err("fcntl failed".into());
            }
        }

        // Drain the file descriptor of all data before registering with Tokio
        loop {
            let mut buf = [0; 4];
            match nfd.read_exact(&mut buf) {
                Ok(_) => {
                    continue;
                }
                Err(e) => {
                    if e.kind() == std::io::ErrorKind::WouldBlock {
                        break;
                    } else {
                        return Err(format!("unexpected read io error {}", e).into());
                    }
                }
            }
        }

        let t = nfd.token;

        // Register the file descriptor with tokio
        let afd = AsyncFd::with_interest(nfd, Interest::READABLE)?;

        // Resume events
        unsafe {
            let r = notify_resume(t);
            if r != 0 {
                return Err("notify_resume failed".into());
            }
        }

        return Ok(Self {
            inner: afd,
            token: t,
        });
    }
}

impl AsyncRead for AsyncNotifyFd {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        loop {
            let mut guard = ready!(self.inner.poll_read_ready_mut(cx))?;
            let r = guard.try_io(|x| x.get_mut().read(buf.initialize_unfilled()));
            if r.is_ok() {
                return Poll::Ready(r.unwrap().map(|r| buf.advance(r)));
            }
        }
    }
}

extern "C" {
    pub fn notify_register_file_descriptor(
        name: *const c_char,
        notify_fd: *mut c_int,
        flags: c_int,
        out_token: *mut c_int,
    ) -> u32;

    pub fn notify_cancel(token: c_int) -> u32;

    // Added to allow safely setting the fd to non blocking mode
    pub fn notify_suspend(token: ::std::os::raw::c_int) -> u32;
    pub fn notify_resume(token: ::std::os::raw::c_int) -> u32;
}
